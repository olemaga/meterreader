import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.xml.bind.DatatypeConverter;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;
import org.opencv.ml.CvKNearest;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
/**
 * @author olemagnus
 * Class extracting the digits before sending them to Knn-class
 *
 */

public class Recognizer {
	  static{ System.loadLibrary(Core.NATIVE_LIBRARY_NAME); }

	private int number;
	private ArrayList<Rect> rectangles;
	public Recognizer(){
		String file = "training/dig.akerdig.exp74.jpg";
		Mat image = new Mat();
		Filter filter = new Filter(file);
		rectangles = filter.getRectangles();
		image = filter.getFilteredMat();
		createKnnImage(image);
	}
	public static void main(String[] args) {

		Recognizer rec = new Recognizer();
	}
	
	
	public static double diffSum(ArrayList<Double> item){
		double sum = 0;
		for (double value : item) {
			sum += value;
		}
		return sum;
	}

	public static double getLowestDiffs(ArrayList<Double> item){
		double lowest = item.get(0);
		for (double value : item) {
			if(value < lowest)
				lowest = value;
		}
		return lowest;
	}
	public Double[] calculateHumoments(Mat image){
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();    
	    Imgproc.findContours(image, contours, new Mat(), Imgproc.RETR_LIST,Imgproc.CHAIN_APPROX_SIMPLE);
	    int index = 0;
	    for(int i=0; i< contours.size();i++){
	        if (Imgproc.contourArea(contours.get(i)) > 8000 ){
	        	index = i;
	            Rect rect = Imgproc.boundingRect(contours.get(i));
	            //Core.rectangle(copyImage, new Point(rect.x,rect.y), new Point(rect.x+rect.width,rect.y+rect.height),new Scalar(0,255,255));
	        }
	    }
		Moments mom = new Moments();
		mom = Imgproc.moments(contours.get(index), false);
		Mat hu = new Mat();
		Imgproc.HuMoments(mom, hu);
		double[] huMoments = new double[(int) (hu.total() * hu.channels())];
		hu.get(0, 0, huMoments);
		Double[] result = new Double[7];
		for (int i = 0; i < huMoments.length; i++) {
			result[i] = Double.valueOf(huMoments[i]);
		}
		return result;
	}
	public void createKnnImage(Mat orgMat){
		int x = Integer.MAX_VALUE;
		int y = Integer.MAX_VALUE;
		int low = 0;
		int right= 0;
		for (Rect rect: rectangles) {
			if(rect.x < x)
				x = rect.x;
			if(rect.y < y)
				y = rect.y;
			if(rect.y+rect.height > low)
				low = rect.y+rect.height;
			if(rect.x+rect.width > right)
				right = rect.x+rect.width;
		}
		Mat mat = new Mat();
		orgMat.copyTo(mat);
		Core.rectangle(mat, new Point(0, 0), new Point(mat.width(), mat.height()), new Scalar(255, 255, 255), Core.FILLED);
		for (Rect rect : rectangles) {
			Rect tempRect = new Rect(new Point(rect.x -5, rect.y-5), new Point(rect.x+rect.width+5, rect.y+rect.height+5));
			Mat subMat = orgMat.submat(tempRect);
			int scale = 2;
			int width = subMat.width()/scale;
			int height = subMat.height()/scale;
			Size sz = new Size(width, height);
			Mat rz = new Mat();
			Imgproc.resize(subMat, rz, sz);
			Mat finalImg = new Mat(new Size(110, height), rz.type(), new Scalar(255, 255, 255));
			int spacing = (110 - width)/2;
			Rect finalRect = new Rect(new Point(spacing, 0), new Point(width+spacing, height));
			rz.copyTo(finalImg.submat(finalRect));
			Knn knn = new Knn(finalImg);
		}

	}
	public Mat getResizedMat(Mat mat){
		Size size = getBiggestSize();
		Mat finalImg = new Mat(size, CvType.CV_8UC1);
		Core.rectangle(finalImg, new Point(0,0), new Point(size.width, size.height), new Scalar(255, 255, 255), Core.FILLED);
		int wOffset = ((int) (size.width - mat.width())/2);
		int hOffset = ((int) (size.height - mat.height())/2);
		Rect roi = new Rect(new Point(wOffset, hOffset), new Point(size.width-wOffset, size.height-hOffset));
		Mat resizeimage = new Mat();
		Size sz = new Size(roi.width,roi.height);
		Imgproc.resize(mat, resizeimage, sz);
		resizeimage.copyTo(new Mat(finalImg, roi));
		return finalImg;
	}
	public Size getBiggestSize(){
		int bigHeight = 0;
		int bigWidth = 0;
		for (Rect rect : rectangles) {
			if(rect.height > bigHeight)
				bigHeight = rect.height;
			if(rect.width > bigWidth)
				bigWidth = rect.width;
		}
		Size size = new Size((double) bigWidth+20, (double) bigHeight+20);
		return size;
	}
	public static void showResult(Mat img) {
	    MatOfByte matOfByte = new MatOfByte();
	    Highgui.imencode(".jpg", img, matOfByte);
	    byte[] byteArray = matOfByte.toArray();
	    BufferedImage bufImage = null;
	    try {
	        InputStream in = new ByteArrayInputStream(byteArray);
	        bufImage = ImageIO.read(in);
	        JFrame frame = new JFrame();
	        frame.getContentPane().add(new JLabel(new ImageIcon(bufImage)));
	        frame.pack();
	        frame.setVisible(true);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	

}
