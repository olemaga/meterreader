import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

import org.opencv.core.Core;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
/**
 * @author olemagnus
 * Simple class just iterating over the training data and finding interesting stats.
 * Primarily used for testing
 */

public class Stats {
	  static{ System.loadLibrary(Core.NATIVE_LIBRARY_NAME); }

	private ArrayList<Integer> values;
	private int margin = 20;
	public Stats(){
		getSum(6);
	}
	public static void main(String[] args) {
		Stats stat = new Stats();
	}
	public void checkLength(int digit, String field){
		int low = Integer.MAX_VALUE;
		int high = 0;
		int number = 0;
		int samples = 0;
		Gson gson = new Gson();
		JsonParser parser = new JsonParser();
		try {
			Object obj = parser.parse(new FileReader("traindata/trainingdata.json"));
			JsonArray array = (JsonArray) obj;
			for (int j = 0; j < array.size(); j++) {
				JsonObject object = array.get(j).getAsJsonObject();
				int num = object.get("number").getAsInt();
				int arclen = object.get("sum1").getAsInt();
				if(num==digit && arclen < low){
					low = arclen;
					number = num;
				}
				else if(num==digit && arclen > high){
					high = arclen;
					number = num;
				}
				if(num==digit)
					samples++;
			}
		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(digit+": low "+low/10000+", high "+high/10000);
		values.add(low-margin);
		values.add(high+margin);
	}
	public void getSum(int digit){
		Gson gson = new Gson();
		int sumfirst = 0;
		int maxfirst = 0;
		int minfirst = Integer.MAX_VALUE;
		
		int sumsec = 0;
		int maxsec = 0;
		int minsec = Integer.MAX_VALUE;
		
		int sumthird = 0;
		int maxthird = 0;
		int minthird = Integer.MAX_VALUE;
		
		int sumfourth = 0;
		int maxfourth = 0;
		int minfourth = Integer.MAX_VALUE;
		int count = 0;
		
		JsonParser parser = new JsonParser();
		try {
			Object obj = parser.parse(new FileReader("traindata/trainingdata.json"));
			JsonArray array = (JsonArray) obj;
			for (int j = 0; j < array.size(); j++) {
				JsonObject object = array.get(j).getAsJsonObject();
				int num = object.get("number").getAsInt();
				int sum1 = object.get("sum1").getAsInt();
				int sum2 = object.get("sum2").getAsInt();
				int sum3 = object.get("sum3").getAsInt();
				int sum4 = object.get("sum4").getAsInt();
				if(num==digit){
					count++;
					sumfirst += sum1;
					sumsec += sum2;
					sumthird += sum3;
					sumfourth += sum4;
					if(sum1 > maxfirst)
						maxfirst = sum1;
					if(sum1 < minfirst)
						minfirst = sum1;
					
					if(sum2 > maxsec)
						maxsec = sum2;
					if(sum2 < minsec)
						minsec = sum2;
					
					if(sum3 > maxthird)
						maxthird = sum3;
					if(sum3 < minthird)
						minthird = sum3;
					
					if(sum4 > maxfourth)
						maxfourth = sum4;
					if(sum4 < minfourth)
						minfourth = sum4;
				}
			}
		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("number of digits: "+count);
		System.out.println("digit avgSum1 maxSum1 minSum1 avgSum2 maxSum2 minSum2 avgSum3 maxSum3 minSum3 avgSum4 maxSum4 minSum4");
		System.out.println(digit+" "+sumfirst/count+" "+maxfirst+" "+minfirst
				+" "+sumsec/count+" "+maxsec+" "+minsec+" "
				+sumthird/count+" "+maxthird+" "+minthird+" "
				+sumfourth/count+" "+maxfourth+" "+minfourth);
	}
	
	
	
}
