import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;
import org.opencv.objdetect.HOGDescriptor;
/**
 * @author olemagnus
 * Class doing feature-related calculations. Remember to copy Mats when doing findContour().
 *
 */

public class Features {
	public static int[] divideImage(Mat org){
		Mat mat = new Mat();
		org.copyTo(mat);
		int centerWidth = (int) mat.width()/2;
		int centerHeight = (int) mat.height()/2;
		Mat firstQuad = mat.submat(new Rect(new Point(0,0), new Point(centerWidth, centerHeight)));
		Mat secQuad = mat.submat(new Rect(new Point(centerWidth,0), new Point(mat.width(), centerHeight)));
		Mat thirdQuad = mat.submat(new Rect(new Point(0,centerHeight), new Point(centerWidth, mat.height())));
		Mat fourthQuad = mat.submat(new Rect(new Point(centerWidth, centerHeight), new Point(mat.width(), mat.height())));
		
		int firstSum = getPixelValues(firstQuad);
		int secSum = getPixelValues(secQuad);
		int thirdSum = getPixelValues(thirdQuad);
		int fourthSum = getPixelValues(fourthQuad);
		int[] sums = {firstSum, secSum, thirdSum, fourthSum};
		return sums;
	}
	public static int getPixelValues(Mat mat){
		Mat copy = new Mat();
		mat.copyTo(copy);
		Mat inverted = new Mat();
		Core.bitwise_not(copy, inverted);
		double[] values = new double[(int) inverted.rows() * mat.cols()];
		int index = 0;
		for (int i = 0; i < inverted.rows(); i++) {
			for (int j = 0; j < inverted.cols(); j++) {
				double[] pixel = inverted.get(i, j);
				values[index] = pixel[0];
				index++;
			}
		}
		double sum = 0;
		for (int i = 0; i < values.length; i++) {
			sum += values[i];
		}
		return (int) sum/values.length;
	}
	public static float[] getHog(Mat mat){
		HOGDescriptor hog = new HOGDescriptor(new Size(64, 128), new Size(16,
	            16), new Size(8, 8), new Size(8, 8), 9, 0, -1,0, 0.2, false, HOGDescriptor.DEFAULT_NLEVELS);
		Mat resized = new Mat();
		Imgproc.resize(mat, resized, new Size(64, 128));
		MatOfFloat desc = new MatOfFloat();
		hog.compute(resized, desc);
		float[] array = new float[desc.rows()*desc.cols()];
		int index = 0;
		for (int i = 0; i < desc.cols(); i++) {
			for (int j = 0; j < desc.rows(); j++) {
				float[] value = new float[1];
				desc.get(j, i, value);
				array[index] = value[0];
				index++;
			}
		}
		return array;
	}
	public static String floatArrayToString(float[] array){
		String string = "";
		for (int i = 0; i < array.length; i++) {
			string+=array[i]+" ";
		}
		return string;
	}
	public static float[] stringToFloatArray(String string){
		String[] array = string.split(" ");
		float[] floatArray = new float[array.length];
		for (int i = 0; i < array.length; i++) {
			floatArray[i] = Float.parseFloat(array[i]);
		}
		return floatArray;
		
	}
	// requirement: has to be six or nine
	public static boolean isSix(Mat mat){
		Mat copy = new Mat();
		mat.copyTo(copy);
		List<MatOfPoint> contours = new ArrayList<>();
		Mat outputHierarchy = new Mat();
		Imgproc.findContours(mat, contours, outputHierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_NONE );
		int currentHeight = 0;
		MatOfPoint foundContour = new MatOfPoint();
		Rect tempRect = new Rect();
		for(int i=0; i< contours.size();i++){
	        if (Imgproc.contourArea(contours.get(i)) > 50 ){
	            Rect rect = Imgproc.boundingRect(contours.get(i));
	            if(rectIsDigit(mat.width(), mat.height(), rect)){
	        	    if(rect.height > currentHeight){
	        	    	currentHeight = rect.height;
	        	    	foundContour = contours.get(i);
	        	    	tempRect = rect;
	        	    }
	            }
		    }
		}
		Moments p = Imgproc.moments(foundContour);
		int x = (int) (p.get_m10() / p.get_m00());
        int y = (int) (p.get_m01() / p.get_m00());
        Core.circle(copy, new Point(x, y), 4, new Scalar(0,49,0,255));
        double ratio = (double) y/tempRect.height;
        if(ratio > 0.5){
        	Training.writeToFile("sixcentroid.jpg", copy);
        	return true;
        }
    	Training.writeToFile("ninecentroid.jpg", copy);
        return false;
	}
	public static void findContours(Mat mat){
		Mat copy = new Mat();
		mat.copyTo(copy);
		List<MatOfPoint> contours = new ArrayList<>();
		Mat outputHierarchy = new Mat();
		Imgproc.findContours(copy, contours, outputHierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_NONE );
		int currentHeight = 0;
		MatOfPoint foundContour = new MatOfPoint();
		Rect tempRect = new Rect();
		for(int i=0; i< contours.size();i++){
	        if (Imgproc.contourArea(contours.get(i)) > 50 ){
	            Rect rect = Imgproc.boundingRect(contours.get(i));
	        	Core.rectangle(copy, new Point(rect.x,rect.y), new Point(rect.x+rect.width,rect.y+rect.height),new Scalar(255,255,255), 2);
	            if(rectIsDigit(copy.width(), copy.height(), rect)){
	        	    if(rect.height > currentHeight){
	        	    	currentHeight = rect.height;
	        	    	foundContour = contours.get(i);
	        	    	tempRect = rect;
	        	    }
	            }
		    }
		}
	
	}
	public static boolean rectIsDigit(int orgWidth, int orgHeight, Rect rect){
		int circleHeight = (int) (orgHeight * 0.65);
		if(rect.width < orgWidth-10 && rect.height < orgHeight-10 && rect.height > circleHeight){
			return true;
		}
		return false;
	}
}
