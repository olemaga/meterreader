package models;
/**
 * @author olemagnus
 * Not used anymore
 *
 */
public class Shape {
	public Shape(boolean isNine, boolean isSixOrEigth, int arcLength) {
		this.isNine = isNine;
		this.isSixOrEigth = isSixOrEigth;
		this.arcLength = arcLength;
	}
	public boolean isNine() {
		return isNine;
	}
	public void setNine(boolean isNine) {
		this.isNine = isNine;
	}
	public boolean isSixOrEight() {
		return isSixOrEigth;
	}
	public void setSixOrEight(boolean isSixOrEigth) {
		this.isSixOrEigth = isSixOrEigth;
	}
	public int getArcLength() {
		return arcLength;
	}
	public void setArcLength(int arcLength) {
		this.arcLength = arcLength;
	}
	private boolean isNine = false;
	private boolean isSixOrEigth = false;
	private int arcLength;
}
