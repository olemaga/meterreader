import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.xml.bind.DatatypeConverter;

import models.Shape;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonWriter;



/**
 * @author olemagnus
 *
 */
public class Training {
	  static{ System.loadLibrary(Core.NATIVE_LIBRARY_NAME); }

	private ArrayList<Rect> rectangles;
	private HashMap<Integer, Integer> countList;
	private static final int maxDigits = 6; // 
	private static String labelData = "traindata/labelsAll.json";
	private ArrayList<Integer[]> labels;
	private static final int expectedDigits = 6;
	
	public Training(String imagefile){
//		getLabelData();
//		filterImages();
//		makeBoxFiles();
		//filterImage(imagefile);
		filterImages();

	}
	public void checkLabels(){
		getLabelData();
		for (Integer[] reading : labels) {
			for (Integer integer : reading) {
				System.out.print(integer+", ");
			}
			System.out.println("");
		}
	}
	ArrayList<Integer> usedNumbers = new ArrayList<Integer>();
	public void filterImages(){
		countList = new HashMap<Integer, Integer>();
		getLabelData();
		for (int i = 11; i < 53; i++) { // indexing starts at 11 to match the filenames
			String filename = "training/dig.akerdig.exp"+i+".jpg";
			Filter filter = new Filter(filename);
			rectangles = filter.getRectangles();
			if(rectangles.size()!=expectedDigits)
				System.out.println("----*****-----NB"+rectangles.size());
			Mat filtered = filter.getFilteredMat();
			int index = i - 11;
			isolateNumbers(filtered, labels.get(index));
		}
		System.out.println("Training finished");

	}
	public void filterImage(String file){
		Filter filter = new Filter(file);
		rectangles = filter.getRectangles();
		if(rectangles.size()!=6)
			System.out.println("Error: Couldn't find 6 digits");
		Mat filtered = filter.getFilteredMat();
		//isolateNumbers(filtered);

	}
	public static void writeToFile(String file, Mat mat){
		Highgui.imwrite(file, mat);
	}
	public static void main(String[] args) {
		if(args.length == 0){
			System.out.println("No image provided. Please add image 'java  -jar PreProcessor.jar <imagefile>");
		}
		else if(args.length > 1){
			System.out.println("Can only handle one image at a time");
		}
		else{
			Training proc = new Training(args[0]); 
		}
	}
	public void humomentsImage(Mat mat){
		Mat currentImage = mat;
		Moments mom = new Moments();
		mom = Imgproc.moments(currentImage, false);
		Mat hu = new Mat();
		Imgproc.HuMoments(mom, hu);
		double[] huMoments = new double[(int) (hu.total() * hu.channels())];
		hu.get(0, 0, huMoments);
		System.out.println("--nytt tall");
		for (int i = 0; i < huMoments.length; i++) {
			System.out.println(huMoments[i]);
		}
	}
	public void isolateNumbers(Mat mat, Integer[] numbers){
		ArrayList<Mat> images = new ArrayList<Mat>();
		for (Rect roi : rectangles) {
			Mat digitImage = mat.submat(roi);
			images.add(digitImage);
		}
		makeFeature(mat, images, numbers);
	}
	public void makeFeature( Mat orgMat, ArrayList<Mat> images, Integer[] numbers){
		ArrayList<Mat> finals = new ArrayList<Mat>();
		for (Mat mat : images) {
			Mat resized = getResizedMat(mat);
			finals.add(resized);
		}
		createTessImage(orgMat, finals, numbers);
	}
	public void createTessImage(Mat orgMat, ArrayList<Mat> images, Integer[] numbers){
		int x = Integer.MAX_VALUE;
		int y = Integer.MAX_VALUE;
		int low = 0;
		int right= 0;
		for (Rect rect: rectangles) {
			if(rect.x < x)
				x = rect.x;
			if(rect.y < y)
				y = rect.y;
			if(rect.y+rect.height > low)
				low = rect.y+rect.height;
			if(rect.x+rect.width > right)
				right = rect.x+rect.width;
		}
		Mat mat = new Mat();
		orgMat.copyTo(mat);
		Core.rectangle(mat, new Point(0, 0), new Point(mat.width(), mat.height()), new Scalar(255, 255, 255), Core.FILLED);
		for (Rect rect : rectangles) {
			Rect tempRect = new Rect(new Point(rect.x -5, rect.y-5), new Point(rect.x+rect.width+5, rect.y+rect.height+5));
			Mat subMat = orgMat.submat(tempRect);
			
			int scale = 2;
			int width = subMat.width()/scale;
			int height = subMat.height()/scale;
			Size sz = new Size(width, height);
			Mat rz = new Mat();
			Imgproc.resize(subMat, rz, sz);
			
			Mat finalImg = new Mat(new Size(110, height), rz.type(), new Scalar(255, 255, 255));
			int spacing = (110 - width)/2;
			Rect finalRect = new Rect(new Point(spacing, 0), new Point(width+spacing, height));
			rz.copyTo(finalImg.submat(finalRect));
			if(canWriteNumber(numbers[rectangles.indexOf(rect)]))
				createFeatures(finalImg, numbers[rectangles.indexOf(rect)]);
		}
	}

	public void writePicture(Mat mat){
		Highgui.imwrite("tesstraining/imageFinal.jpg", mat);
	}
	public void appendImage(Mat matToAppend){
		Mat orgMat = Highgui.imread("tesstraining/trainingimage.jpg", 0);
		Mat newMat = new Mat();
		if(orgMat.total()==0){ // if no image exists. 
			orgMat = matToAppend;
			
			Highgui.imwrite("tesstraining/trainingimage.jpg", orgMat);
		}
		else{ 
			int width = 0;
			if(matToAppend.width() > orgMat.width()) // selects the biggest width
				width = matToAppend.width();
			else
				width = orgMat.width();
			
			int height = orgMat.height()+matToAppend.height(); // total height is the two images added together
			newMat = new Mat(new Size(width, height), orgMat.type(), new Scalar(255, 255, 255));
		
			Mat orgTarget = newMat.submat(new Rect(new Point(0, 0), new Point(orgMat.width(), orgMat.height())));
			orgMat.copyTo(orgTarget); //copy old image to upper area
			
			Mat newTarget = newMat.submat(new Rect(new Point(0, orgMat.height()), new Point(matToAppend.width(), newMat.height())));
			matToAppend.copyTo(newTarget); // copy new image to bottom area
			
			System.out.println("mattoappend: "+matToAppend.type() +", orgmat: "+orgMat.type());
			
			Highgui.imwrite("tesstraining/trainingimage.jpg", newMat);
		}
	}
	public Mat getResizedMat(Mat mat){
		Size size = getBiggestSize();
		Mat finalImg = new Mat(size, CvType.CV_8UC1);
		Core.rectangle(finalImg, new Point(0,0), new Point(size.width, size.height), new Scalar(255, 255, 255), Core.FILLED);
		int wOffset = ((int) (size.width - mat.width())/2);
		int hOffset = ((int) (size.height - mat.height())/2);
		Rect roi = new Rect(new Point(wOffset, hOffset), new Point(size.width-wOffset, size.height-hOffset));
		Mat resizeimage = new Mat();
		Size sz = new Size(roi.width,roi.height);
		Imgproc.resize(mat, resizeimage, sz);
		resizeimage.copyTo(new Mat(finalImg, roi));
		return finalImg;
	}
	public Size getBiggestSize(){
		int bigHeight = 0;
		int bigWidth = 0;
		for (Rect rect : rectangles) {
			if(rect.height > bigHeight)
				bigHeight = rect.height;
			if(rect.width > bigWidth)
				bigWidth = rect.width;
		}
		Size size = new Size((double) bigWidth+20, (double) bigHeight+20);
		return size;
	}
	public void createFeatures(Mat mat, int number){
		int centerWidth = (int) mat.width()/2;
		int centerHeight = (int) mat.height()/2;
		Mat firstQuad = mat.submat(new Rect(new Point(0,0), new Point(centerWidth, centerHeight)));
		Mat secQuad = mat.submat(new Rect(new Point(centerWidth,0), new Point(mat.width(), centerHeight)));
		Mat thirdQuad = mat.submat(new Rect(new Point(0,centerHeight), new Point(centerWidth, mat.height())));
		Mat fourthQuad = mat.submat(new Rect(new Point(centerWidth, centerHeight), new Point(mat.width(), mat.height())));
		
		int firstSum = getPixelValues(firstQuad);
		int secSum = getPixelValues(secQuad);
		int thirdSum = getPixelValues(thirdQuad);
		int fourthSum = getPixelValues(fourthQuad);
		addToJson(number, firstSum, secSum, thirdSum, fourthSum, mat); 
		numberToMap(number);
	}
	public boolean canWriteNumber(int number){
		if(countList.containsKey(number)){
			if(countList.get(number) == maxDigits){
				return false;
			}
		}
		return true;
	}
	public void numberToMap(int number){
		if(countList.containsKey(number)){
			int value = countList.get(number)+1;
			countList.put(number, value);
		}
		else{
			countList.put(number, 1);
		}
	}
	
	public void addToJson(int number, int sum1, int sum2, int sum3, int sum4, Mat mat){
		Gson gson = new Gson();
		JsonParser parser = new JsonParser();
		try {
			Object obj = parser.parse(new FileReader("traindata/trainingdata.json"));
			JsonArray array = (JsonArray) obj;
			JsonObject object = new JsonObject();
			object.addProperty("number", number);
			object.addProperty("sum1", sum1);
			object.addProperty("sum2", sum2);
			object.addProperty("sum3", sum3);
			object.addProperty("sum4", sum4);
//			object.addProperty("length", shape.getArcLength());
//			object.addProperty("nine", shape.isNine());
//			object.addProperty("sixOrEight", shape.isSixOrEight());
			object.add("data", (JsonObject)parser.parse(matToJson(mat)));
			array.add(object);
			JsonWriter writer = new JsonWriter(new FileWriter("traindata/trainingdata.json"));
			gson.toJson(array, writer);
			writer.flush();
			writer.close();
		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public static int getPixelValues(Mat mat){
		Mat copy = new Mat();
		mat.copyTo(copy);
		Mat inverted = new Mat();
		Core.bitwise_not(copy, inverted);
		double[] values = new double[(int) inverted.rows() * mat.cols()];
		int index = 0;
		for (int i = 0; i < inverted.rows(); i++) {
			for (int j = 0; j < inverted.cols(); j++) {
				double[] pixel = inverted.get(i, j);
				double valueToPut = 0.0;
				values[index] = pixel[0];
				index++;
			}
		}
		double sum = 0;
		for (int i = 0; i < values.length; i++) {
			sum += values[i];
		}
		return (int) sum/values.length;
	}
	public static String matToJson(Mat mat){        
	    JsonObject obj = new JsonObject();

	    if(mat.isContinuous()){
	        int cols = mat.cols();
	        int rows = mat.rows();
	        int elemSize = (int) mat.elemSize();    

	        byte[] data = new byte[cols * rows * elemSize];

	        mat.get(0, 0, data);

	        obj.addProperty("rows", mat.rows()); 
	        obj.addProperty("cols", mat.cols()); 
	        obj.addProperty("type", mat.type());

	        String dataString2 = new String(DatatypeConverter.printBase64Binary(data));
	        obj.addProperty("data", dataString2);            
	        Gson gson = new Gson();
	        String json = gson.toJson(obj);
	      
	        return json;
	    } else {
	    	System.out.println("Mat not continous");
	    }
	    return "{}";
	}
	public static void writeToFile(String filename, String input){
		PrintWriter out = null;
		try {
			out = new PrintWriter(new BufferedWriter(new FileWriter("traindata/"+filename, true)));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		out.println(input);
	    out.close();
	}
	public void getLabelData(){
		labels = new ArrayList<Integer[]>();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File(labelData)));
			String line;
			while ((line = reader.readLine()) != null){
				String[] digits = line.split(",");
				Integer[] integers = new Integer[digits.length];
				for (int i = 0; i < digits.length; i++) {
					integers[i] = Integer.parseInt(digits[i]);
				}
				labels.add(integers);
			}
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public int getLength(Mat mat, int number){
		List<MatOfPoint> contours = new ArrayList<>();
		Mat outputHierarchy = new Mat();
		Mat canny = new Mat();
		mat.copyTo(canny);
	    int arcLength = 0;
		Imgproc.findContours(canny, contours, outputHierarchy, Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_NONE );
		for(int i=0; i< contours.size();i++){
	        if (Imgproc.contourArea(contours.get(i)) > 50 ){
	            Rect rect = Imgproc.boundingRect(contours.get(i));
	            if(rect.height > 230 && rect.height < 400 && rect.width < 300){
	            	if(!isRectOfImage(mat.width(), rect.width, mat.height(), rect.height)){
		            	MatOfPoint2f length = new MatOfPoint2f(contours.get(i).toArray());
		            	double dist = Imgproc.arcLength(length, false);
		            	arcLength = (int) dist;
	            	}
	            }
	        }
		 }

	return arcLength;
	}
	public boolean isRectOfImage(int w1, int w2, int h1, int h2){
		if((w1-w2) < 5 && (h1-h2) < 5){
			return true;
		}
		return false;
	}
	public boolean isNaN(double x){
		return x != x;
	}
	
	// Shows mat-image in JFrame
	public static void showResult(Mat img) {
	    MatOfByte matOfByte = new MatOfByte();
	    Highgui.imencode(".jpg", img, matOfByte);
	    byte[] byteArray = matOfByte.toArray();
	    BufferedImage bufImage = null;
	    try {
	        InputStream in = new ByteArrayInputStream(byteArray);
	        bufImage = ImageIO.read(in);
	        JFrame frame = new JFrame();
	        frame.getContentPane().add(new JLabel(new ImageIcon(bufImage)));
	        frame.pack();
	        frame.setVisible(true);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
}
