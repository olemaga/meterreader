import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
/**
 * @author olemagnus
 * Class used by both PreProcessor and Recognizer to filter the image, and it also provides a List with the bounding rectangles.
 *
 */

public class Filter {

	private ArrayList<Rect> rectangles;
	private Mat filteredMat;

	public Filter(String file){
		rectangles = new ArrayList<Rect>();
		Mat cropped = getCroppedMat(file);
		Mat orgCopy = new Mat();
		cropped.copyTo(orgCopy);
		cropped = invertImage(cropped);
		cropped = tresholdImage(cropped);
		getContours(cropped);
		removeSmallestRects();
		filteredMat = cropped;
		orgCopy = invertImage(orgCopy);
		dilatePicture(25);
		erodePicture(10);
	}
	public ArrayList<Rect> getRectangles(){
		return rectangles;
	}
	public Mat getFilteredMat(){
		return filteredMat;
	}
	public void erodePicture(int value){
		Imgproc.erode(filteredMat, filteredMat, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(value, value)));
		//eroded.copyTo(filteredMat.submat(rectangles.get(rectangles.size()-1)));
	}
	public void tresholdLastDigit(Mat mat){
		Mat sub = mat.submat(rectangles.get(rectangles.size()-1));
		Mat tresholded = new Mat();
		double treshValue = 250;
		double maxValue = 255;
		Imgproc.threshold(sub, tresholded, treshValue, maxValue, Imgproc.THRESH_BINARY_INV);
		tresholded.copyTo(filteredMat.submat(rectangles.get(rectangles.size()-1)));
	}
	public void dilatePicture(int value){
		Imgproc.dilate(filteredMat, filteredMat, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(value, value)));
		Training.writeToFile("latex/dilated.jpg", filteredMat);
	}
	public Mat invertImage(Mat mat){
		Mat inverted = new Mat();
	    Imgproc.cvtColor(mat, inverted, Imgproc.COLOR_RGB2GRAY);
	    Mat imgCopy = new Mat();
		inverted.copyTo(imgCopy);
		return imgCopy;
	}
	public Mat getCroppedMat(String file){
		Mat mat = new Mat();
		mat = Highgui.imread(file);
		Point topLeft = new Point(250.0, 500.0);
		Point bottomRight = new Point(2585.0, 1400.0);
		Mat cropped = new Mat(mat.size(), mat.type());
		cropped = new Mat(mat, new Rect(topLeft, bottomRight));
		return cropped;
	}
	public Mat tresholdImage(Mat mat){
		Mat tresholded = new Mat();
		double treshValue = 170;
		double maxValue = 255;
		Imgproc.threshold(mat, tresholded, treshValue, maxValue, Imgproc.THRESH_BINARY_INV);
		return tresholded;
	}
	public void printRectangles(Mat mat){
		for (Rect rect : rectangles) {
        	Core.rectangle(mat, new Point(rect.x,rect.y), new Point(rect.x+rect.width,rect.y+rect.height),new Scalar(0,100,255), 3);
		}
	}
	
	public void getContours(Mat image){
		Mat imageA = image;
		Mat copy = new Mat();
		imageA.copyTo(copy);
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();    
	    Imgproc.findContours(copy, contours, new Mat(), Imgproc.RETR_LIST,Imgproc.CHAIN_APPROX_SIMPLE);
	    for(int i=0; i< contours.size();i++){
	    	  if (Imgproc.contourArea(contours.get(i)) > 5000 ){
	            Rect rect = Imgproc.boundingRect(contours.get(i));
            	if (rect.height < 30 && rect.width < 30){
	            }
	            if(rect.height > 230 && rect.height < 400 && rect.width < 300){
	            	Rect newRect = new Rect(new Point(rect.x, rect.y), new Point(rect.x+rect.width, rect.y+rect.height));
	            	rectangles.add(newRect);
	            }
	        }
	    }
		Training.writeToFile("latex/contours.jpg", copy);
		
		// cleans the uninteresting areas of the image. Not really necessary, but fine for showing pics.
	    Rect lower = getLowerArea(copy);
	    Core.rectangle(copy, new Point(lower.x,lower.y), new Point(lower.x+lower.width,lower.y+lower.height),new Scalar(255,255,255), Core.FILLED);
	    Rect upper = getUpperArea(copy);
	    Core.rectangle(copy, new Point(upper.x,upper.y), new Point(upper.x+upper.width,upper.y+upper.height),new Scalar(255,255,255), Core.FILLED);
	    Rect right = getRightArea(copy);
	    Core.rectangle(copy, new Point(right.x,right.y), new Point(right.x+right.width,right.y+right.height),new Scalar(255,255,255), Core.FILLED);
	    Rect left = getLeftArea(copy);
	    Core.rectangle(copy, new Point(left.x,left.y), new Point(left.x+left.width,left.y+left.height),new Scalar(255,255,255), Core.FILLED);
	}
	public boolean isOuterRectangle(Mat mat, Rect rect){
		double sum = 0.0;
		double limit = 100.0;
		int widthToCheck = 10;
		for (int i = rect.x; i < rect.x+rect.width; i++) {
			for (int j = rect.y-widthToCheck; j < rect.y ; j++) {
				byte[] pixel = new byte[1];
				mat.get(i, j, pixel);
				sum+= pixel[0];
			}
		}
		System.out.println(sum);
		if(Math.abs(sum) > limit){
        	//Core.rectangle(mat, new Point(rect.x,rect.y), new Point(rect.x+rect.width,rect.y+rect.height),new Scalar(255,255,255), Core.FILLED);
        	return false;
		}
		return true;
	}
	public void removeSmallestRects(){
		if(rectangles.size()>6){
			int smallestHeight = rectangles.get(0).height;
			int index = 0;
			for (Rect rect : rectangles) {
				if(rect.height < smallestHeight){
					smallestHeight = rect.height;
					index = rectangles.indexOf(rect);
				}
			}
			rectangles.remove(index);
			if(rectangles.size() > 6)
				removeSmallestRects();
		}
		else if(rectangles.size() < 6){
			System.out.println("less than 6 rectangles detected");
		}
		sortRectangles();
	}
	public void sortRectangles(){
		 Collections.sort(rectangles, new Comparator<Rect>() {
		        @Override public int compare(Rect r1, Rect r2) {
		            return r1.x- r2.x;
		        }

		    });
	}
	public Rect getLowerArea(Mat fullSize){
		int lowestY = 0;
		for (Rect rect: rectangles) {
			if(rect.y+rect.height > lowestY)
				lowestY = rect.y+rect.height;
		}
		Rect bottomRect = new Rect(new Point(0, lowestY+5), new Point(fullSize.width(), fullSize.height()));
		return bottomRect;
	}
	public Rect getUpperArea(Mat fullSize){
		int highestY = 999;
		for (Rect rect: rectangles) {
			if(rect.y < highestY)
				highestY = rect.y;
		}
		Rect upperRect = new Rect(new Point(0, 0), new Point(fullSize.width(), highestY-5));
		return upperRect;
	}
	public Rect getRightArea(Mat fullSize){
		Rect highestX = new Rect(new Point(0,0), new Point(1,1));
		for (Rect rect: rectangles) {
			if(rect.x > highestX.x)
				highestX = rect;
		}
		int rightX = highestX.x+highestX.width;
		Rect rightRect = new Rect(new Point(rightX+45, 0), new Point(fullSize.width(), fullSize.height()));
		return rightRect;
	}
	public Rect getLeftArea(Mat fullSize){
		int lowestX = 999;
		for (Rect rect: rectangles) {
			if(rect.x < lowestX)
				lowestX = rect.x;
		}
		Rect leftRect = new Rect(new Point(0, 0), new Point(lowestX-5, fullSize.height()));
		return leftRect;
	}
	public void writeToFile(String filename, String input){
		PrintWriter out = null;
		try {
			out = new PrintWriter(new BufferedWriter(new FileWriter("latex/"+filename, true)));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		out.println(input);
	    out.close();
	}
}
