import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.xml.bind.DatatypeConverter;

import models.Shape;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
/**
 * @author olemagnus
 * Class doing Knn, humoments-matching, pixelsum-matching, etc.
 *
 */

public class Knn {
	  static{ System.loadLibrary(Core.NATIVE_LIBRARY_NAME); }

	private HashMap<Integer, Integer> map = new HashMap<Integer, Integer>(); //number, count;
	private ArrayList<Integer> excluded = new ArrayList<Integer>();
	private ArrayList<Integer> temp = new ArrayList<Integer>();
	private int lowerTreshold = 0;
	private double distTreshold = 0.0;
	private int k = 3; // k-neighbors
	private double matchTreshold = 0.0;
	private int firstSum;
	private int secSum;
	private int thirdSum;
	private int fourthSum;
	private Stats stats;
	private ArrayList<Integer> allowedDigits;
	
	int count = 0;
	public Knn(Mat mat){
		//divideImage(mat);
		int number = getMatches(mat);
		System.out.println("number found: "+number);
	}
	public int getMatches(Mat mat){
		Mat copy = new Mat();
		mat.copyTo(copy);
		int number = 0;
		if(isOne(copy)){
			return 1;
		}
		else{
			ArrayList<Integer> neighbors = new ArrayList<Integer>();
			for (int i = 0; i < 3; i++) {
				neighbors.add(findMatch(mat));
			}
			HashMap<Integer, Integer> map = new HashMap<Integer, Integer>(); //value, count;
			for (int i = 0; i < neighbors.size(); i++) {
				if(map.containsKey(neighbors.get(i))){
					int count = map.get(neighbors.get(i)) + 1;
					map.put(neighbors.get(i), count);
				}
				else{
					map.put(neighbors.get(i), 1);
				}	
			}
			int highestCount = 0;
			int bestNumber = 0;
			for(Map.Entry<Integer, Integer> entry : map.entrySet()){
				Integer value = entry.getKey();
				Integer count = entry.getValue();
				if(count > highestCount){
					highestCount = count;
					bestNumber = value;
				}
			}
			if(highestCount > 1){
				number = bestNumber;
			}
			else{
				number = neighbors.get(0);
			}
		}
		Mat temp = new Mat();
		mat.copyTo(temp);
		if(number==6 || number == 9){
			boolean six = Features.isSix(temp);
			if(six)
				number =  6;
			else
				number = 9;
		}
		else if(number == 2 || number == 5){
			number = divideImage(temp);
		}
		matchTreshold = 0;
		return number;
	}
	public static int getPixelValues(Mat mat){
		Mat copy = new Mat();
		mat.copyTo(copy);
		Mat inverted = new Mat();
		Core.bitwise_not(copy, inverted);
		double[] values = new double[(int) inverted.rows() * mat.cols()];
		int index = 0;
		for (int i = 0; i < inverted.rows(); i++) {
			for (int j = 0; j < inverted.cols(); j++) {
				double[] pixel = inverted.get(i, j);
				double valueToPut = 0.0;
				if(pixel[0]==255.0)
					valueToPut = 0.0;
				else
					valueToPut = 1.0;
				values[index] = valueToPut;
				index++;
			}
		}
		double sum = 0;
		for (int i = 0; i < values.length; i++) {
			sum += values[i];
		}
		return (int) sum;
	}
	public int divideImage(Mat mat){
		int[] sums = Features.divideImage(mat);
		int neighbor1 = getNearestNeighbors(sums[0], "sum1");
		int neighbor2 = getNearestNeighbors(sums[1], "sum2");
		int neighbor3 = getNearestNeighbors(sums[2], "sum3");
		int neighbor4 = getNearestNeighbors(sums[3], "sum4");
		System.out.println(neighbor1+" "+neighbor2+" "+neighbor3+" "+neighbor4);
		ArrayList<Integer> neighbors = new ArrayList<Integer>();
		int candidate1 = allowedDigits.get(0);
		int candidate2 = allowedDigits.get(1);
		int sum1 = 0;
		int sum2 = 0;
		neighbors.add(neighbor1);neighbors.add(neighbor2);neighbors.add(neighbor3);neighbors.add(neighbor4);
		for (Integer number : neighbors) {
			if(number==candidate1)
				sum1++;
			else
				sum2++;
		}
		if(sum1 > sum2){
			return candidate1;
		}
		return candidate2;
	}
	public int getNearestHog(float[] hog){
		ArrayList<Integer> neighbors = new ArrayList<Integer>();
		for (int i = 0; i < k; i++) {
			int neighbor = hogEuclideanDist(hog);
			neighbors.add(neighbor);
		}
		distTreshold = 0;
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>(); //value, count;
		for (int i = 0; i < neighbors.size(); i++) {
			if(map.containsKey(neighbors.get(i))){
				int count = map.get(neighbors.get(i)) + 1;
				map.put(neighbors.get(i), count);
			}
			else{
				map.put(neighbors.get(i), 1);
			}	
		}
		int highestCount = 0;
		int bestNumber = 0;
		for(Map.Entry<Integer, Integer> entry : map.entrySet()){
			Integer value = entry.getKey();
			Integer count = entry.getValue();
			if(count > highestCount){
				highestCount = count;
				bestNumber = value;
			}
		}
		if(highestCount > 1)
			return bestNumber;
		return neighbors.get(0);
	}
	public int getNearestNeighbors(int sum, String field){
		allowedDigits = new ArrayList<Integer>();
		allowedDigits.add(5);
		allowedDigits.add(2);
		ArrayList<Integer> neighbors = new ArrayList<Integer>();
		for (int i = 0; i < 1; i++) {
			int neighbor = findNeighbor(sum, field);
			neighbors.add(neighbor);
		}
		lowerTreshold = 0;
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>(); //value, count;
		for (int i = 0; i < neighbors.size(); i++) {
			if(map.containsKey(neighbors.get(i))){
				int count = map.get(neighbors.get(i)) + 1;
				map.put(neighbors.get(i), count);
			}
			else{
				map.put(neighbors.get(i), 1);
			}	
		}
		int highestCount = 0;
		int bestNumber = 0;
		for(Map.Entry<Integer, Integer> entry : map.entrySet()){
			Integer value = entry.getKey();
			Integer count = entry.getValue();
			if(count > highestCount){
				highestCount = count;
				bestNumber = value;
			}
		}
		if(highestCount > 1)
			return bestNumber;
		return neighbors.get(0);
	}
	public int findNeighbor(int pixelsum, String field){
		int lowestDiff = Integer.MAX_VALUE;
		int number = 0;
		Gson gson = new Gson();
		JsonParser parser = new JsonParser();
		try {
			Object obj = parser.parse(new FileReader("traindata/trainingdata.json"));
			JsonArray array = (JsonArray) obj;
			for (int i = 0; i < array.size(); i++) {
				JsonObject object = array.get(i).getAsJsonObject();
				int num = object.get("number").getAsInt();
				if(allowedDigits.contains(num)){
					int sum = object.get(field).getAsInt();
					int diff = Math.abs(pixelsum-sum);
					if(diff < lowestDiff && diff > lowerTreshold){
						lowestDiff = diff;
						number = num;
					}
				}
			}
		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lowerTreshold = lowestDiff;
		return number;
		
	}
	public int hogEuclideanDist(float[] hogArray){
		double lowDist = Double.MAX_VALUE;
		int number = 0;
		Gson gson = new Gson();
		JsonParser parser = new JsonParser();
		try {
			Object obj = parser.parse(new FileReader("traindata/trainingdata.json"));
			JsonArray array = (JsonArray) obj;
			for (int i = 0; i < array.size(); i++) {
				JsonObject object = array.get(i).getAsJsonObject();
				int num = object.get("number").getAsInt();
				String hog = object.get("hog").getAsString();
				float[] tempArray = Features.stringToFloatArray(hog);
				double distance = 0;
				for (int j = 0; j < hogArray.length; j++) {
					if(hogArray[i]==tempArray[i]){
					}
					distance += Math.abs(hogArray[i] - tempArray[i]);
				}
				if(distance < lowDist && distance > distTreshold){
					lowDist = distance;
					number = num;
				}
			}
		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		distTreshold = lowDist;
		return number;
	}

	public int getHighestCount(){
		Entry<Integer,Integer> maxEntry = null;
		for(Entry<Integer,Integer> entry : map.entrySet()) {
		    if (maxEntry == null || entry.getValue() > maxEntry.getValue()) {
		        maxEntry = entry;
		    }
		}
		return maxEntry.getKey();
	}

	public void findLowest(int pixelsum, String field){
		int lowestDiff = pixelsum;
		int number = 0;
		Gson gson = new Gson();
		JsonParser parser = new JsonParser();
		try {
			Object obj = parser.parse(new FileReader("traindata/trainingdata.json"));
			JsonArray array = (JsonArray) obj;
			for (int i = 0; i < array.size(); i++) {
				JsonObject object = array.get(i).getAsJsonObject();
				int num = object.get("number").getAsInt();
				int sum = object.get(field).getAsInt();
				int diff = Math.abs(pixelsum-sum);
				if(temp.size() > 0){
					if(!temp.contains(num) && diff < lowestDiff && diff > lowerTreshold){
						lowestDiff = diff;
						number = num;
					}
				}
				else{
					if(diff < lowestDiff && diff > lowerTreshold){
						lowestDiff = diff;
						number = num;
					}
				}
			}
		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lowerTreshold = lowestDiff;
		if(map.get(number) == null)
			map.put(number, 1);
		else{
			int value = map.get(number)+1;
			map.put(number, value);
		}			
	}
	
	public static void main(String[] args) {
		System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
	}
	public int findMatch(Mat mat){
		int number = 0;
		double best = Double.MAX_VALUE;
		Gson gson = new Gson();
		JsonParser parser = new JsonParser();
		Mat temp = new Mat();
		try {
			Object obj = parser.parse(new FileReader("traindata/trainingdata.json"));
			JsonArray array = (JsonArray) obj;
			for (int i = 0; i <array.size() ; i++) { 
				JsonObject object = array.get(i).getAsJsonObject();
				int num = object.get("number").getAsInt();
				JsonObject imgString  = object.get("data").getAsJsonObject();
				Mat img = matFromJson(imgString.toString());
				double matchValue = matchShapes(mat, img);
				if(matchValue < best && matchValue > matchTreshold){
					best = matchValue;
					number = num;
					temp = img;
				}
			}
		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		matchTreshold = best;
		return number;
	}
	public boolean isOne(Mat mat){
		List<MatOfPoint> contours = new ArrayList<>();
		Mat outputHierarchy = new Mat();
		Imgproc.findContours(mat, contours, outputHierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_NONE );
		int currentHeight = 0;
		MatOfPoint foundContour = new MatOfPoint();
		Rect tempRect = new Rect();
		for(int i=0; i< contours.size();i++){
	        if (Imgproc.contourArea(contours.get(i)) > 50 ){
	            Rect rect = Imgproc.boundingRect(contours.get(i));
	            if(rectIsDigit(mat.width(), mat.height(), rect)){
	        	    if(rect.height > currentHeight){
	        	    	currentHeight = rect.height;
	        	    	foundContour = contours.get(i);
	        	    	tempRect = rect;
	        	    }
	            }
		    }
		}
		if(tempRect.width < 40){
			return true;
		}
		return false;
	}
	public MatOfPoint getCountour(Mat org){
		Mat copy = new Mat();
		org.copyTo(copy);
		List<MatOfPoint> contours = new ArrayList<>();
		Mat outputHierarchy = new Mat();
		Imgproc.findContours(copy, contours, outputHierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_NONE );
		int currentHeight = 0;
		MatOfPoint foundContour = new MatOfPoint();
		Rect tempRect = new Rect();
		for(int i=0; i< contours.size();i++){
	        if (Imgproc.contourArea(contours.get(i)) > 50 ){
	            Rect rect = Imgproc.boundingRect(contours.get(i));
	            if(rectIsDigit(copy.width(), copy.height(), rect)){
	        	    if(rect.height > currentHeight){
	        	    	currentHeight = rect.height;
	        	    	foundContour = contours.get(i);
	        	    	tempRect = rect;
	        	    }
	            }
		    }
		}
		return foundContour;
	}
	public double matchShapes(Mat org, Mat newImg){
		MatOfPoint orgShape = getCountour(org);
		MatOfPoint newShape = getCountour(newImg);
		double value = 0.0;
		value = Imgproc.matchShapes(orgShape, newShape, Imgproc.CV_CONTOURS_MATCH_I1, 0.0);
		if(value==0.0)
			System.out.println("Match returned 0.0");
		return value;
	}
	public boolean rectIsDigit(int orgWidth, int orgHeight, Rect rect){
		int circleHeight = (int) (orgHeight * 0.65);
		if(rect.width < orgWidth-10 && rect.height < orgHeight-10 && rect.height > circleHeight){
			return true;
		}
		return false;
	}
	
	public static Mat matFromJson(String json){
	    JsonParser parser = new JsonParser();
	    JsonObject JsonObject = parser.parse(json).getAsJsonObject();
	    
	    int rows = JsonObject.get("rows").getAsInt();
	    int cols = JsonObject.get("cols").getAsInt();
	    int type = JsonObject.get("type").getAsInt();

	    String dataString = JsonObject.get("data").getAsString();
	    byte[] decoded = DatatypeConverter.parseBase64Binary(dataString);
	  //  byte[] data = Base64.decode(dataString.getBytes(), Base64.); 

	    Mat mat = new Mat(rows, cols, type);
	    mat.put(0, 0, decoded);

	    return mat;
	}
	public boolean isRectOfImage(int w1, int w2, int h1, int h2){
		if((w1-w2) < 5 && (h1-h2) < 5){
			return true;
		}
		return false;
	}
}
