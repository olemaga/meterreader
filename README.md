OCR of digits from electricity meter readings
------
+ Ooops! Java classes use hardcoded strings for filenames/folder (Training.java & Recognizer.java) certain places due to testing.
+ Some save locations might also be hardcoded at the moment.

### Description of important classes

+ __Training.java__ - Processes images and saves feature data in json-file
+ __Filter.java__ - Tresholding, dilation, isolation, etc.
+ __Recognizer.java__ - Formats images (resizing, etc), and passes them to Knn
+ __Knn.java__ - Performs Knn with Hu moments, pixel sums, and so on

Other classes are used testing and statistics